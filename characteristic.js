var util = require('util');
var bleno = require('bleno');

var Gpio = require('onoff').Gpio
var led = new Gpio(17, 'out');

var BlenoCharacteristic = bleno.Characteristic;

var LedCharacteristic = function() {
  LedCharacteristic.super_.call(this, {
    uuid: 'ff11',
    properties: ['read', 'write', 'notify'],
    value: null
  });

  this._value = new Buffer(0);
  this._updateValueCallback = null;
};

util.inherits(LedCharacteristic, BlenoCharacteristic);

LedCharacteristic.prototype.onReadRequest = function(offset, callback) {
  var data = new Buffer(1);
  data[0] = led.readSync();

  console.log('LedCharacteristic - onReadRequest: value = ' + String(data[0]));

  callback(this.RESULT_SUCCESS, this._value);
};

LedCharacteristic.prototype.onWriteRequest = function(data, offset, withoutResponse, callback) {
  console.log('LedCharacteristic - onWriteRequest');

  led.writeSync(data[0]);

  if (this._updateValueCallback) {
    console.log('LedCharacteristic - onWriteRequest: notifying');

    this._updateValueCallback(this._value);
  }

  callback(this.RESULT_SUCCESS);
};

LedCharacteristic.prototype.onSubscribe = function(maxValueSize, updateValueCallback) {
  console.log('LedCharacteristic - onSubscribe');

  this._updateValueCallback = updateValueCallback;
};

LedCharacteristic.prototype.onUnsubscribe = function() {
  console.log('LedCharacteristic - onUnsubscribe');

  this._updateValueCallback = null;
};

module.exports = LedCharacteristic;


function exit() {
  led.unexport();
  process.exit();
}
process.on('SIGINT', exit);