var bleno = require('bleno');

var BlenoPrimaryService = bleno.PrimaryService;

var LedCharacteristic = require('./characteristic');

console.log('bleno - led');

bleno.on('stateChange', function(state) {
  console.log('on -> stateChange: ' + state);

  if (state === 'poweredOn') {
    bleno.startAdvertising('led controller', ['ff00']);
  } else {
    bleno.stopAdvertising();
  }
});

bleno.on('advertisingStart', function(error) {
  console.log('on -> advertisingStart: ' + (error ? 'error ' + error : 'success'));

  if (!error) {
    bleno.setServices([
      new BlenoPrimaryService({
        uuid: 'ff00',
        characteristics: [
          new LedCharacteristic()
        ]
      })
    ]);
  }
});
